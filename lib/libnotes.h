/* libnotes.h generated by valac 0.56.17, the Vala compiler, do not modify */

#ifndef __LIBNOTES_H__
#define __LIBNOTES_H__

#include <gtk/gtk.h>
#include <glib-object.h>
#include <cairo-gobject.h>
#include <glib.h>
#include <gio/gio.h>
#include <stdlib.h>
#include <string.h>
#include <gtksourceview/gtksource.h>
#include <gdk/gdk.h>

G_BEGIN_DECLS

#if !defined(VALA_EXTERN)
#if defined(_MSC_VER)
#define VALA_EXTERN __declspec(dllexport) extern
#elif __GNUC__ >= 4
#define VALA_EXTERN __attribute__((visibility("default"))) extern
#else
#define VALA_EXTERN extern
#endif
#endif

#define XNP_TYPE_ICON_BUTTON (xnp_icon_button_get_type ())
#define XNP_ICON_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), XNP_TYPE_ICON_BUTTON, XnpIconButton))
#define XNP_ICON_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), XNP_TYPE_ICON_BUTTON, XnpIconButtonClass))
#define XNP_IS_ICON_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XNP_TYPE_ICON_BUTTON))
#define XNP_IS_ICON_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), XNP_TYPE_ICON_BUTTON))
#define XNP_ICON_BUTTON_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), XNP_TYPE_ICON_BUTTON, XnpIconButtonClass))

typedef struct _XnpIconButton XnpIconButton;
typedef struct _XnpIconButtonClass XnpIconButtonClass;
typedef struct _XnpIconButtonPrivate XnpIconButtonPrivate;
typedef enum  {
	XNP_TITLE_BAR_BUTTON_TYPE_EMPTY,
	XNP_TITLE_BAR_BUTTON_TYPE_CLOSE,
	XNP_TITLE_BAR_BUTTON_TYPE_LEFT_ARROW,
	XNP_TITLE_BAR_BUTTON_TYPE_RIGHT_ARROW,
	XNP_TITLE_BAR_BUTTON_TYPE_REFRESH
} XnpTitleBarButtonType;

#define XNP_TYPE_TITLE_BAR_BUTTON_TYPE (xnp_title_bar_button_type_get_type ())

#define XNP_TYPE_TITLE_BAR_BUTTON (xnp_title_bar_button_get_type ())
#define XNP_TITLE_BAR_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), XNP_TYPE_TITLE_BAR_BUTTON, XnpTitleBarButton))
#define XNP_TITLE_BAR_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), XNP_TYPE_TITLE_BAR_BUTTON, XnpTitleBarButtonClass))
#define XNP_IS_TITLE_BAR_BUTTON(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XNP_TYPE_TITLE_BAR_BUTTON))
#define XNP_IS_TITLE_BAR_BUTTON_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), XNP_TYPE_TITLE_BAR_BUTTON))
#define XNP_TITLE_BAR_BUTTON_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), XNP_TYPE_TITLE_BAR_BUTTON, XnpTitleBarButtonClass))

typedef struct _XnpTitleBarButton XnpTitleBarButton;
typedef struct _XnpTitleBarButtonClass XnpTitleBarButtonClass;
typedef struct _XnpTitleBarButtonPrivate XnpTitleBarButtonPrivate;

#define XNP_TYPE_WINDOW_MONITOR (xnp_window_monitor_get_type ())
#define XNP_WINDOW_MONITOR(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), XNP_TYPE_WINDOW_MONITOR, XnpWindowMonitor))
#define XNP_WINDOW_MONITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), XNP_TYPE_WINDOW_MONITOR, XnpWindowMonitorClass))
#define XNP_IS_WINDOW_MONITOR(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XNP_TYPE_WINDOW_MONITOR))
#define XNP_IS_WINDOW_MONITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), XNP_TYPE_WINDOW_MONITOR))
#define XNP_WINDOW_MONITOR_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), XNP_TYPE_WINDOW_MONITOR, XnpWindowMonitorClass))

typedef struct _XnpWindowMonitor XnpWindowMonitor;
typedef struct _XnpWindowMonitorClass XnpWindowMonitorClass;
typedef struct _XnpWindowMonitorPrivate XnpWindowMonitorPrivate;

#define XNP_TYPE_WINDOW (xnp_window_get_type ())
#define XNP_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), XNP_TYPE_WINDOW, XnpWindow))
#define XNP_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), XNP_TYPE_WINDOW, XnpWindowClass))
#define XNP_IS_WINDOW(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XNP_TYPE_WINDOW))
#define XNP_IS_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), XNP_TYPE_WINDOW))
#define XNP_WINDOW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), XNP_TYPE_WINDOW, XnpWindowClass))

typedef struct _XnpWindow XnpWindow;
typedef struct _XnpWindowClass XnpWindowClass;

#define XNP_TYPE_APPLICATION (xnp_application_get_type ())
#define XNP_APPLICATION(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), XNP_TYPE_APPLICATION, XnpApplication))
#define XNP_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), XNP_TYPE_APPLICATION, XnpApplicationClass))
#define XNP_IS_APPLICATION(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XNP_TYPE_APPLICATION))
#define XNP_IS_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), XNP_TYPE_APPLICATION))
#define XNP_APPLICATION_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), XNP_TYPE_APPLICATION, XnpApplicationClass))

typedef struct _XnpApplication XnpApplication;
typedef struct _XnpApplicationClass XnpApplicationClass;
typedef struct _XnpApplicationPrivate XnpApplicationPrivate;

#define XNP_TYPE_HYPERTEXT_VIEW (xnp_hypertext_view_get_type ())
#define XNP_HYPERTEXT_VIEW(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), XNP_TYPE_HYPERTEXT_VIEW, XnpHypertextView))
#define XNP_HYPERTEXT_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), XNP_TYPE_HYPERTEXT_VIEW, XnpHypertextViewClass))
#define XNP_IS_HYPERTEXT_VIEW(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XNP_TYPE_HYPERTEXT_VIEW))
#define XNP_IS_HYPERTEXT_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), XNP_TYPE_HYPERTEXT_VIEW))
#define XNP_HYPERTEXT_VIEW_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), XNP_TYPE_HYPERTEXT_VIEW, XnpHypertextViewClass))

typedef struct _XnpHypertextView XnpHypertextView;
typedef struct _XnpHypertextViewClass XnpHypertextViewClass;
typedef struct _XnpHypertextViewPrivate XnpHypertextViewPrivate;

#define XNP_TYPE_NOTE (xnp_note_get_type ())
#define XNP_NOTE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), XNP_TYPE_NOTE, XnpNote))
#define XNP_NOTE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), XNP_TYPE_NOTE, XnpNoteClass))
#define XNP_IS_NOTE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XNP_TYPE_NOTE))
#define XNP_IS_NOTE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), XNP_TYPE_NOTE))
#define XNP_NOTE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), XNP_TYPE_NOTE, XnpNoteClass))

typedef struct _XnpNote XnpNote;
typedef struct _XnpNoteClass XnpNoteClass;
typedef struct _XnpNotePrivate XnpNotePrivate;
typedef struct _XnpWindowPrivate XnpWindowPrivate;

#define XNP_TYPE_THEME (xnp_theme_get_type ())
#define XNP_THEME(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), XNP_TYPE_THEME, XnpTheme))
#define XNP_THEME_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), XNP_TYPE_THEME, XnpThemeClass))
#define XNP_IS_THEME(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XNP_TYPE_THEME))
#define XNP_IS_THEME_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), XNP_TYPE_THEME))
#define XNP_THEME_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), XNP_TYPE_THEME, XnpThemeClass))

typedef struct _XnpTheme XnpTheme;
typedef struct _XnpThemeClass XnpThemeClass;
typedef struct _XnpThemePrivate XnpThemePrivate;

#define XNP_TYPE_THEME_GTKCSS (xnp_theme_gtkcss_get_type ())
#define XNP_THEME_GTKCSS(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), XNP_TYPE_THEME_GTKCSS, XnpThemeGtkcss))
#define XNP_THEME_GTKCSS_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), XNP_TYPE_THEME_GTKCSS, XnpThemeGtkcssClass))
#define XNP_IS_THEME_GTKCSS(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), XNP_TYPE_THEME_GTKCSS))
#define XNP_IS_THEME_GTKCSS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), XNP_TYPE_THEME_GTKCSS))
#define XNP_THEME_GTKCSS_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), XNP_TYPE_THEME_GTKCSS, XnpThemeGtkcssClass))

typedef struct _XnpThemeGtkcss XnpThemeGtkcss;
typedef struct _XnpThemeGtkcssClass XnpThemeGtkcssClass;
typedef struct _XnpThemeGtkcssPrivate XnpThemeGtkcssPrivate;

struct _XnpIconButton {
	GtkEventBox parent_instance;
	XnpIconButtonPrivate * priv;
	gboolean active;
};

struct _XnpIconButtonClass {
	GtkEventBoxClass parent_class;
	void (*draw_icon) (XnpIconButton* self, cairo_t* cr, gint width, gint height);
};

struct _XnpTitleBarButton {
	XnpIconButton parent_instance;
	XnpTitleBarButtonPrivate * priv;
};

struct _XnpTitleBarButtonClass {
	XnpIconButtonClass parent_class;
};

struct _XnpWindowMonitor {
	GObject parent_instance;
	XnpWindowMonitorPrivate * priv;
	XnpWindow* window;
};

struct _XnpWindowMonitorClass {
	GObjectClass parent_class;
};

struct _XnpApplication {
	GObject parent_instance;
	XnpApplicationPrivate * priv;
	gboolean system_tray_mode;
};

struct _XnpApplicationClass {
	GObjectClass parent_class;
};

struct _XnpHypertextView {
	GtkSourceView parent_instance;
	XnpHypertextViewPrivate * priv;
};

struct _XnpHypertextViewClass {
	GtkSourceViewClass parent_class;
};

struct _XnpNote {
	GtkScrolledWindow parent_instance;
	XnpNotePrivate * priv;
	XnpHypertextView* text_view;
	gulong save_handler_id;
	gulong tab_handler_id;
	gboolean backed;
};

struct _XnpNoteClass {
	GtkScrolledWindowClass parent_class;
};

struct _XnpWindow {
	GtkWindow parent_instance;
	XnpWindowPrivate * priv;
};

struct _XnpWindowClass {
	GtkWindowClass parent_class;
};

struct _XnpTheme {
	GObject parent_instance;
	XnpThemePrivate * priv;
};

struct _XnpThemeClass {
	GObjectClass parent_class;
};

struct _XnpThemeGtkcss {
	GObject parent_instance;
	XnpThemeGtkcssPrivate * priv;
};

struct _XnpThemeGtkcssClass {
	GObjectClass parent_class;
};

VALA_EXTERN GType xnp_icon_button_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XnpIconButton, g_object_unref)
VALA_EXTERN void xnp_icon_button_draw_icon (XnpIconButton* self,
                                cairo_t* cr,
                                gint width,
                                gint height);
VALA_EXTERN void xnp_icon_button_set_widget_source_color (XnpIconButton* self,
                                              cairo_t* cr);
VALA_EXTERN XnpIconButton* xnp_icon_button_construct (GType object_type);
VALA_EXTERN gboolean xnp_icon_button_get_enabled (XnpIconButton* self);
VALA_EXTERN void xnp_icon_button_set_enabled (XnpIconButton* self,
                                  gboolean value);
VALA_EXTERN GType xnp_title_bar_button_type_get_type (void) G_GNUC_CONST ;
VALA_EXTERN GType xnp_title_bar_button_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XnpTitleBarButton, g_object_unref)
VALA_EXTERN XnpTitleBarButton* xnp_title_bar_button_new (XnpTitleBarButtonType icon_type);
VALA_EXTERN XnpTitleBarButton* xnp_title_bar_button_construct (GType object_type,
                                                   XnpTitleBarButtonType icon_type);
VALA_EXTERN XnpTitleBarButtonType xnp_title_bar_button_get_icon_type (XnpTitleBarButton* self);
VALA_EXTERN void xnp_title_bar_button_set_icon_type (XnpTitleBarButton* self,
                                         XnpTitleBarButtonType value);
VALA_EXTERN GType xnp_window_monitor_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XnpWindowMonitor, g_object_unref)
VALA_EXTERN GType xnp_window_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XnpWindow, g_object_unref)
VALA_EXTERN XnpWindowMonitor* xnp_window_monitor_new (XnpWindow* window,
                                          GFile* file);
VALA_EXTERN XnpWindowMonitor* xnp_window_monitor_construct (GType object_type,
                                                XnpWindow* window,
                                                GFile* file);
VALA_EXTERN GType xnp_application_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XnpApplication, g_object_unref)
VALA_EXTERN XnpApplication* xnp_application_new (const gchar* config_file);
VALA_EXTERN XnpApplication* xnp_application_construct (GType object_type,
                                           const gchar* config_file);
VALA_EXTERN XnpApplication* xnp_application_new_with_notes_path (const gchar* config_file,
                                                     const gchar* notes_path);
VALA_EXTERN XnpApplication* xnp_application_construct_with_notes_path (GType object_type,
                                                           const gchar* config_file,
                                                           const gchar* notes_path);
VALA_EXTERN void xnp_application_quit (XnpApplication* self);
VALA_EXTERN XnpWindow* xnp_application_create_window (XnpApplication* self,
                                          const gchar* name);
VALA_EXTERN gboolean xnp_application_save_windows_configuration (XnpApplication* self);
VALA_EXTERN void xnp_application_save_notes (XnpApplication* self);
VALA_EXTERN GSList* xnp_application_get_window_list (XnpApplication* self);
VALA_EXTERN void xnp_application_show_hide_notes (XnpApplication* self);
VALA_EXTERN void xnp_application_open_settings_dialog (XnpApplication* self);
VALA_EXTERN void xnp_application_open_about_dialog (XnpApplication* self);
VALA_EXTERN GtkMenu* xnp_application_context_menu (XnpApplication* self);
VALA_EXTERN const gchar* xnp_application_get_notes_path (XnpApplication* self);
VALA_EXTERN void xnp_application_set_notes_path (XnpApplication* self,
                                     const gchar* value);
VALA_EXTERN const gchar* xnp_application_get_config_file (XnpApplication* self);
VALA_EXTERN gboolean xnp_application_get_skip_taskbar_hint (XnpApplication* self);
VALA_EXTERN void xnp_application_set_skip_taskbar_hint (XnpApplication* self,
                                            gboolean value);
VALA_EXTERN XnpWindow* xnp_application_get_next_focus (XnpApplication* self);
VALA_EXTERN GType xnp_hypertext_view_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XnpHypertextView, g_object_unref)
VALA_EXTERN XnpHypertextView* xnp_hypertext_view_new (void);
VALA_EXTERN XnpHypertextView* xnp_hypertext_view_construct (GType object_type);
VALA_EXTERN void xnp_hypertext_view_undo (XnpHypertextView* self);
VALA_EXTERN void xnp_hypertext_view_redo (XnpHypertextView* self);
VALA_EXTERN void xnp_hypertext_view_update_tags (XnpHypertextView* self);
VALA_EXTERN const gchar* xnp_hypertext_view_get_font (XnpHypertextView* self);
VALA_EXTERN void xnp_hypertext_view_set_font (XnpHypertextView* self,
                                  const gchar* value);
VALA_EXTERN GType xnp_note_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XnpNote, g_object_unref)
VALA_EXTERN void xnp_note_save (XnpNote* self);
VALA_EXTERN XnpNote* xnp_note_new (const gchar* name);
VALA_EXTERN XnpNote* xnp_note_construct (GType object_type,
                             const gchar* name);
VALA_EXTERN const gchar* xnp_note_get_name (XnpNote* self);
VALA_EXTERN void xnp_note_set_name (XnpNote* self,
                        const gchar* value);
VALA_EXTERN gchar* xnp_note_get_text (XnpNote* self);
VALA_EXTERN void xnp_note_set_text (XnpNote* self,
                        const gchar* value);
VALA_EXTERN gboolean xnp_note_get_dirty (XnpNote* self);
VALA_EXTERN void xnp_note_set_dirty (XnpNote* self,
                         gboolean value);
VALA_EXTERN XnpWindow* xnp_window_new (XnpApplication* app);
VALA_EXTERN XnpWindow* xnp_window_construct (GType object_type,
                                 XnpApplication* app);
VALA_EXTERN void xnp_window_hide (XnpWindow* self);
VALA_EXTERN void xnp_window_get_geometry (XnpWindow* self,
                              gint* winx,
                              gint* winy,
                              gint* width,
                              gint* height);
VALA_EXTERN gint xnp_window_compare_func (XnpWindow* self,
                              XnpWindow* win2);
VALA_EXTERN gint xnp_window_get_current_page (XnpWindow* self);
VALA_EXTERN void xnp_window_set_current_page (XnpWindow* self,
                                  gint page);
VALA_EXTERN void xnp_window_popup_error (XnpWindow* self,
                             const gchar* message);
VALA_EXTERN void xnp_window_dialog_hide (XnpWindow* self,
                             GtkDialog* dialog);
VALA_EXTERN void xnp_window_dialog_destroy (XnpWindow* self,
                                GtkDialog* dialog);
VALA_EXTERN XnpNote* xnp_window_insert_note (XnpWindow* self,
                                 const gchar* name);
VALA_EXTERN void xnp_window_connect_note_signals (XnpWindow* self,
                                      XnpNote* note,
                                      GtkEventBox* tab_evbox);
VALA_EXTERN void xnp_window_disconnect_note_signals (XnpWindow* self,
                                         XnpNote* note,
                                         GtkEventBox* tab_evbox);
VALA_EXTERN void xnp_window_move_note (XnpWindow* self,
                           const gchar* note_name,
                           gint page);
VALA_EXTERN gchar** xnp_window_get_note_names (XnpWindow* self,
                                   gint* result_length1);
VALA_EXTERN GtkEventBox* xnp_window_get_tab_evbox (XnpWindow* self,
                                       XnpNote* note);
VALA_EXTERN void xnp_window_delete_current_note (XnpWindow* self);
VALA_EXTERN void xnp_window_rename_current_note (XnpWindow* self);
VALA_EXTERN void xnp_window_save_notes (XnpWindow* self);
VALA_EXTERN const gchar* xnp_window_get_name (XnpWindow* self);
VALA_EXTERN void xnp_window_set_name (XnpWindow* self,
                          const gchar* value);
VALA_EXTERN XnpNote* xnp_window_get_current_note (XnpWindow* self);
VALA_EXTERN gint xnp_window_get_n_pages (XnpWindow* self);
VALA_EXTERN gboolean xnp_window_get_empty (XnpWindow* self);
VALA_EXTERN gboolean xnp_window_get_show_tabs (XnpWindow* self);
VALA_EXTERN void xnp_window_set_show_tabs (XnpWindow* self,
                               gboolean value);
VALA_EXTERN gint xnp_window_get_tabs_position (XnpWindow* self);
VALA_EXTERN void xnp_window_set_tabs_position (XnpWindow* self,
                                   gint value);
VALA_EXTERN gboolean xnp_window_get_above (XnpWindow* self);
VALA_EXTERN void xnp_window_set_above (XnpWindow* self,
                           gboolean value);
VALA_EXTERN gboolean xnp_window_get_sticky (XnpWindow* self);
VALA_EXTERN void xnp_window_set_sticky (XnpWindow* self,
                            gboolean value);
VALA_EXTERN gboolean xnp_window_get_show_refresh_button (XnpWindow* self);
VALA_EXTERN void xnp_window_set_show_refresh_button (XnpWindow* self,
                                         gboolean value);
VALA_EXTERN GType xnp_theme_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XnpTheme, g_object_unref)
VALA_EXTERN XnpTheme* xnp_theme_new (void);
VALA_EXTERN XnpTheme* xnp_theme_construct (GType object_type);
VALA_EXTERN void xnp_theme_use_gtk_style (XnpTheme* self);
VALA_EXTERN void xnp_theme_use_color (XnpTheme* self,
                          const gchar* color);
VALA_EXTERN GType xnp_theme_gtkcss_get_type (void) G_GNUC_CONST ;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (XnpThemeGtkcss, g_object_unref)
VALA_EXTERN XnpThemeGtkcss* xnp_theme_gtkcss_new (void);
VALA_EXTERN XnpThemeGtkcss* xnp_theme_gtkcss_construct (GType object_type);
VALA_EXTERN void xnp_theme_gtkcss_update_color_css (XnpThemeGtkcss* self,
                                        GdkRGBA* rgba);
VALA_EXTERN gboolean xnp_theme_gtkcss_get_use_gtk_style (XnpThemeGtkcss* self);
VALA_EXTERN void xnp_theme_gtkcss_set_use_gtk_style (XnpThemeGtkcss* self,
                                         gboolean value);

G_END_DECLS

#endif
