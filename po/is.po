# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Xfce
# This file is distributed under the same license as the xfce-panel-plugins.xfce4-notes-plugin package.
# 
# Translators:
# Sveinn í Felli <sv1@fellsnet.is>, 2013-2015,2019
# Sveinn í Felli <sv1@fellsnet.is>, 2019
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/\n"
"POT-Creation-Date: 2024-12-24 00:53+0100\n"
"PO-Revision-Date: 2013-07-03 19:09+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>, 2013-2015,2019\n"
"Language-Team: Icelandic (http://app.transifex.com/xfce/xfce-panel-plugins/language/is/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: is\n"
"Plural-Forms: nplurals=2; plural=(n % 10 != 1 || n % 100 == 11);\n"

#: src/xfce4-notes-autostart.desktop.in:4 src/xfce4-notes-plugin.desktop.in:5
#: src/xfce4-notes.desktop.in:5 lib/application.vala:313
#: lib/application.vala:1017 lib/window.vala:84 lib/window.vala:194
#: src/main-panel-plugin.vala:46 src/main-status-icon.vala:34
#: src/main-status-icon.vala:107 src/main-settings-dialog.c:99
msgid "Notes"
msgstr "Minnispunktar"

#: src/xfce4-notes-autostart.desktop.in:5 src/xfce4-notes-plugin.desktop.in:6
#: src/xfce4-notes.desktop.in:6 lib/application.vala:1019
msgid "Ideal for your quick notes"
msgstr "Handhægt tól fyrir snöggtekna minnispunkta"

#: lib/application.vala:214
#, c-format
msgid ""
"The selected directory (%s) for the new notes path already contains files. "
"You must select or create an empty directory."
msgstr "Valin mappa (%s) fyrir nýja minnispunkta inniheldur skrár nú þegar. YÞú verður að velja tóma möppu.."

#: lib/application.vala:226
#, c-format
msgid "Unable to select directory for new notes path: %s"
msgstr "Mistókst að velja möppu fyrir slóð á nýja minnispunkta: %s"

#: lib/application.vala:234
msgid "Notes path is unacceptable"
msgstr ""

#: lib/application.vala:237 lib/application.vala:899 lib/application.vala:997
#: lib/window.vala:1232
msgid "Error"
msgstr "Villa"

#: lib/application.vala:317
#, c-format
msgid "Notes %d"
msgstr "Minnispunktar %d"

#: lib/application.vala:338 lib/window.vala:1062
#, c-format
msgid "Note %d"
msgstr ""

#: lib/application.vala:647
msgid "Rename group"
msgstr "Endurnefna hóp"

#: lib/application.vala:667 lib/window.vala:1230
#, c-format
msgid "The name %s is already in use"
msgstr "Nafnið %s er þegar í notkun"

#: lib/application.vala:698
msgid "Are you sure you want to delete this group?"
msgstr "Ertu viss um að þú viljir eyða þessum hópi?"

#: lib/application.vala:763
#, c-format
msgid "The group \"%s\" has been modified on the disk"
msgstr "Hópnum \"%s\" hefur verið breytt á disknum"

#: lib/application.vala:766
msgid "Do you want to reload the group?"
msgstr "Viltu endurhlaða inn hópnum?"

#: lib/application.vala:896
#, c-format
msgid "The name \"%s\" is invalid."
msgstr "Nafnið „%s“ er ógilt."

#: lib/application.vala:897
#, c-format
msgid "The invalid characters are: %s"
msgstr "Ógildir stafir eru: %s"

#: lib/application.vala:994
msgid "Unable to open the settings dialog"
msgstr "Tókst ekki að opna stillingaglugga"

#: lib/application.vala:1026
msgid "translator-credits"
msgstr "Sveinn í Felli, sv1@fellsnet.is"

#: lib/application.vala:1061 lib/window.vala:894
msgid "_Add a new group"
msgstr "Bæt_a við nýjum hópi"

#: lib/window.vala:248
msgid "Menu"
msgstr "Valmynd"

#: lib/window.vala:273
msgid "Refresh notes"
msgstr "Endurlesa minnispunkta"

#: lib/window.vala:288
#, c-format
msgid "Hide (%s)"
msgstr "Fela (%s)"

#: lib/window.vala:812 src/main-panel-plugin.vala:50
#: src/main-status-icon.vala:68
msgid "_Groups"
msgstr "_Hópar"

#: lib/window.vala:823
msgid "_New"
msgstr ""

#: lib/window.vala:824
msgid "_Delete"
msgstr ""

#: lib/window.vala:825
msgid "_Rename"
msgstr "Endu_rnefna"

#. Always on top
#: lib/window.vala:829 src/main-settings-dialog.c:186
msgid "Always on top"
msgstr "Alltaf efst"

#: lib/window.vala:830
msgid "Sticky window"
msgstr "Límdur gluggi"

#: lib/window.vala:834 src/main-status-icon.vala:74
msgid "_Properties"
msgstr ""

#: lib/window.vala:835 src/main-status-icon.vala:75
msgid "_About"
msgstr ""

#: lib/window.vala:892
msgid "_Rename group"
msgstr "Endu_rnefna hóp"

#: lib/window.vala:893
msgid "_Delete group"
msgstr "_Eyða hóp"

#: lib/window.vala:1175
msgid "Are you sure you want to delete this note?"
msgstr "Ertu viss um að þú viljir eyða þessum minnispunkti?"

#: lib/window.vala:1208
msgid "Rename note"
msgstr "Endurnefna minnispunkt"

#: src/main-status-icon.vala:78
msgid "_Quit"
msgstr "_Hætta"

#: src/main-settings-dialog.c:101
msgid "_Close"
msgstr "_Loka"

#: src/main-settings-dialog.c:121
msgid "Global settings"
msgstr "Víðværar stillingar"

#: src/main-settings-dialog.c:129
msgid "Background:"
msgstr "Bakgrunnur:"

#: src/main-settings-dialog.c:142
msgid "Font:"
msgstr "Letur:"

#: src/main-settings-dialog.c:154
msgid "Tabs position:"
msgstr "Staðsetning flipa:"

#. Hide from taskbar
#: src/main-settings-dialog.c:161
msgid "Hide notes from taskbar"
msgstr "Fela minnispunkta frá verkefnaslá"

#: src/main-settings-dialog.c:172
msgid "Notes path:"
msgstr "Slóð á minnispunkta:"

#: src/main-settings-dialog.c:181
msgid "New group settings"
msgstr "Stillingar nýrra hópa"

#. Sticky window
#: src/main-settings-dialog.c:193
msgid "Sticky"
msgstr "Límt"

#: src/main-settings-dialog.c:203
msgid "Size:"
msgstr "Stærð:"

#: src/main-settings-dialog.c:223 src/main-settings-dialog.c:231
msgid "Select notes path"
msgstr "Veldu slóð fyrir minnispunkta"

#: src/main-settings-dialog.c:286
msgid "None"
msgstr "Ekkert"

#: src/main-settings-dialog.c:287
msgid "Top"
msgstr "Efst"

#: src/main-settings-dialog.c:288
msgid "Right"
msgstr "Hægri"

#: src/main-settings-dialog.c:289
msgid "Bottom"
msgstr "Neðst"

#: src/main-settings-dialog.c:290
msgid "Left"
msgstr "Vinstri"

#: src/main-settings-dialog.c:306
msgid "Small"
msgstr "Lítið"

#: src/main-settings-dialog.c:307
msgid "Normal"
msgstr "Venjulegt"

#: src/main-settings-dialog.c:308
msgid "Large"
msgstr "Stórt"

#: src/main-settings-dialog.c:366
msgid "Yellow"
msgstr "Gult"

#: src/main-settings-dialog.c:367
msgid "Blue"
msgstr "Blátt"

#: src/main-settings-dialog.c:368
msgid "Green"
msgstr "Grænt"

#: src/main-settings-dialog.c:369
msgid "Indigo"
msgstr "Djúpfjólublátt"

#: src/main-settings-dialog.c:370
msgid "Olive"
msgstr "Ólífugrænt"

#: src/main-settings-dialog.c:371
msgid "Carmine"
msgstr "Dumbrautt"

#: src/main-settings-dialog.c:372
msgid "Mimosa"
msgstr "Mímósa"

#: src/main-settings-dialog.c:373
msgid "White"
msgstr "Hvítt"

#: src/main-settings-dialog.c:374
msgid "Android"
msgstr "Android"

#: src/main-settings-dialog.c:375
msgid "GTK+"
msgstr "GTK+"

#: src/main-settings-dialog.c:376
msgid "Custom..."
msgstr "Sérsniðið..."

#: src/main-settings-dialog.c:507
msgid "Background Color"
msgstr "Bakgrunnslitur"
